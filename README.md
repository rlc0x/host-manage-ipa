host-manage-ipa
=========

Ansible role allowing the management of FreeIPA settings on a host. Includes adding the host IP address to the reverse lookup zone.

Requirements
------------

None

Role Variables
--------------

Available variables are listed below, look in `defaults/main.yml` for additional variables

    ipa_server: 192.168.100.250

Either the IP address or FQDN of the FreeIPA server joining to.

    ipa_pass: {{ vault_ip_pass }}

Per Ansible best practices store the password as a vault protected value.

    ipa_user: admin

A user with permissions to add objects to the FreeIPA server

    ipa_domain: example.com

The domain the host should be added to

    ptr_zone: 

Dynamically calculated from the host value of the ip address

    nodes: []

A list of node definitions to manage IPA membership

    nodes:
      - name: server1
        ip_address: 192.168.100.2
        fqdn: server1.{{ ipa_domain }}
        state: present or absent
      - name: server2
        ip_address: 192.168.100.3
        fqdn: server2.{{ ipa_domain }}
        state: present or absent

Dependencies
------------

community.general.ipa_dnsrecord

Example Playbook
----------------

```yaml
- name: Manage the IPA registration
  hosts: all
  gather_facts: False
  any_errors_fatal: True
  become: True
  vars:
    ipa_domain: example.com
    ipa_user: admin
    ipa_pass: "{{ vault_ipa_pass }}"
    ipa_server: "ipasrv01.{{ ipa_domain }}"
    nodes:
      - name: server1
        ip_address: 192.168.100.2
        fqdn: server1.{{ ipa_domain }}
        state: present or absent
      - name: server2
        ip_address: 192.168.100.3
        fqdn: server2.{{ ipa_domain }}
        state: present or absent
    roles:
      - host-manage-ipa
```

License
-------

MIT

Author Information
------------------

Raymond Cox
